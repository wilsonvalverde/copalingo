const RosApi = require('node-routeros').RouterOSAPI;

const conn = new RosApi({
    host: '192.168.88.1',
    user: 'admin',
    password: '',
    keepalive: true,
});

module.exports = conn;
