const db = require('mongoose');

const URI = 'mongodb://localhost/' + process.env.DB_NAME;

db.connect(URI, { useNewUrlParser: true})
    .then(res => console.log('Successful connection to the database!'))
    .catch(err => console.log(err));

module.exports = db;
