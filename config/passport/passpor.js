var needle = require('needle');
var bCrypt = require('bcrypt-nodejs');
var Binnacle = require('../../models/binnacle.model');
module.exports = function (passport) {
    var LocalStrategy = require('passport-local').Strategy;
    passport.serializeUser(function (account, done) {
        needle.get(USERSDB_SERVER + 'account/email/' + account.email_account, function (error, response) {
            if (!error && response.statusCode == 200) {
                done(null, response.body.account);
            } else {
                done(error, null);
            }
        });
    });

    // used to deserialize the user
    passport.deserializeUser(function (id, done) {
        if (id) {
            needle.get(USERSDB_SERVER + 'account/email/' + id.email_account, function (error, response) {
                if (!error && response.statusCode == 200) {
                    done(null, response.body.account);
                } else {
                    done(error, null);
                }
            });
        } else {
            console.error("Error: ", id.errors);
            done(id.errors, null);
        }
    });

    passport.use('local-signin', new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'pass',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },



        function (req, email, password, done) {
            var isValidPassword = function (userpass, password) {
                return bCrypt.compareSync(password, userpass);
            };
            needle.get(USERSDB_SERVER + 'account/email/' + email, function (error, response) {

                if (!error && response.statusCode == 200) {
                    console.log(response.body);
                    if (!response.body.account) {
                        console.log("correo no existe");
                        return req.flash('BAD', "Credenciales Incorrectas!", "/dashboard");
                    }
                    if (!response.body.account.active_account) {
                        console.log("su cuenta esta desactivada");
                        return req.flash('BAD', "Su cuenta esta desactivada!", "/dashboard");
                    }
                    if (!isValidPassword(response.body.account.password_account, password)) {
                        console.log("clave incorrecta");
                        return req.flash('BAD', "Credenciales Incorrectas!", "/dashboard");
                    }
                    var userinfo = response.body.account;

                    console.log("EXITO");
                    req.session.name = response.body.account.user.name_user;
                    req.session.lastname = response.body.account.user.last_name_user;
                    var fecha = new Date();
                    const dataBinnacle = {
                        modific_date: fecha,
                        description: "El Administrador " + req.session.name + " " + req.session.lastname + " a iniciado sesion. "
                    }
                    const binnacle = new Binnacle(dataBinnacle);
                    binnacle.save();
                    return done(null, userinfo);

                } else {
                    console.log("Error:                  ", error);
                    console.log("servidor fuera de linea");
                    return done(null, false);
                }
            });
        }
    ));

    //registro de usuarios por passport

    passport.use('local-signup', new LocalStrategy(
        {
            usernameField: 'email_account',//lo que esta como name en el input del registro
            passwordField: 'password_account',//lo que esta como name en el input del registro
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function (req, email, password, done) {
            var name_user = req.body.name_user;
            var last_name_user = req.body.last_name_user;
            var birthday_user = req.body.birthday_user;
            var photo_user = req.body.photo_user;
            var ci_user = req.body.ci_user;
            //validar si ningun campo esta vacio
            if (!email && !password && !name_user && !last_name_user && !ci_user && !photo_user && !birthday_user) {
                console.log('Debe Ingresar Todos los datos');
                return done(null, false);
            } else {
                birthday_user = req.body.birthday_user.split("-")[2] + "-" + req.body.birthday_user.split("-")[1] + "-" + req.body.birthday_user.split("-")[0];
            }
            //validar que el correo sea correcto
            var emailRegEx = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
            if (!emailRegEx.test(email)) {
                console.log('El Correo Ingresado es Incorrecto');
                return done(null, false);
            }
            //comprobar si las contraseñas coinciden
            if (req.body.password_account != req.body.demo_password) {
                console.log('Las Claves no Coinciden');
                return done(null, false);
            }
            //validar que los nombres y apellidos sean solo letras y espacio
            var RegExPattern = /[a-zA-Z ]/;
            console.log(name_user.match(RegExPattern));
            if ((!name_user.match(RegExPattern)) || (!last_name_user.match(RegExPattern))) {
                console.log('Sus Nombres o Apellidos deben contener unicamente letras');
                return done(null, false);
            }
            //validar que la fecha tenga el formato yyyy-mm-dd
            RegExPattern = /^\d{2,4}\-\d{1,2}\-\d{1,2}$/;
            if (!birthday_user.match(RegExPattern)) {
                console.log('El formato de la fecha no es correcto');
                return done(null, false);
            }

            var test_birthday_user = new Date();
            var fecha = birthday_user.split("-");
            test_birthday_user.setFullYear(fecha[0], fecha[1] - 1, fecha[2]);
            var today = new Date();
            var day = fecha[2];
            var month = fecha[1];
            var year = fecha[0];
            var date = new Date(year, month, '0');
            //fecha valida
            if ((day - 0) > (date.getDate() - 0)) {
                console.log('La fecha ingresada es incorrecta');
                return done(null, false);
            }
            //fecha no futura
            var dateNew = today.setFullYear(today.getFullYear() - 5);
            if (test_birthday_user > dateNew) {
                console.log('La fecha ingresada no es una fecha de nacimiento valida');
                return done(null, false);
            }

            //            var generateHash = function (password) {
            //                return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
            //            };
            //verificar si el email no esta registrado y guardar el usuario
            Person.findOne({ dni_person: req.body.ci_user }, function (error_save, findPerson) {
                if (error_save) {
                    console.error(error_save);
                } else {
                    if (!findPerson) {
                        let data_save = {
                            dni_person: req.body.ci_user,
                            type_dni_person: "Cedula",
                            gender_person: req.body.gender_user,
                            first_name_person: req.body.name_user,
                            last_name_person: req.body.last_name_user,
                            dob_person: new Date(birthday_user),
                            nacionality_person: req.body.nacionalidad,
                            address_person: {
                                principal_street: req.body.direccion,
                                number_house: req.body.housen
                            }
                        };
                        let person_save = new Person(data_save);
                        person_save.save(function (error_save, newPerson) {
                            if (error_save) {
                                console.error(error_save);
                            }
                        });
                    }
                }
            });
            needle.get(USERSDB_SERVER + 'account/email/' + email, function (error, response) {
                if (!error && response.statusCode == 200) {
                    if (response.body.account) {
                        console.log('El Correo ya esta Registrado');
                        return done(null, false);
                    } else {
                        var ext_img = "";
                        var base64 = "";
                        if (req.files && req.files.photo_user != null && req.files.photo_user != undefined) {
                            let EDFile = req.files.photo_user;
                            const types_image = {
                                'image/png': 'png',
                                'image/bmp': 'bmp',
                                'image/cis-cod': 'cod',
                                'image/gif': 'gif',
                                'image/ief': 'ief',
                                'image/jpeg': 'jpeg',
                                'image/jpg': 'jpg',
                                'image/pipeg': 'jfif',
                                'image/svg+xml': 'svg',
                                'image/tiff': 'tif',
                                'image/x-cmu-raster': 'ras',
                                'image/x-cmx': 'cmx',
                                'image/x-icon': 'ico',
                                'image/x-portable-anymap': 'pnm',
                                'image/x-portable-bitmap': 'pbm',
                                'image/x-portable-graymap': 'pgm',
                                'image/x-portable-pixmap': 'ppm',
                                'image/x-rgb': 'rgb',
                                'image/x-xbitmap': 'xbm',
                                'image/x-xpixmap': 'xpm',
                                'image/x-xwindowdump': 'xwd'
                            };

                            ext_img = "." + types_image[EDFile.mimetype];
                            var path_img = __dirname.replace("config/passport", "public/images/user/") + req.body.ci_user + "." + types_image[EDFile.mimetype]
                            EDFile.mv(path_img, err => {
                                if (err) console.log(err);
                                console.log("imagen subida");
                                let buff = fs.readFileSync(path_img);
                                base64 = buff.toString('base64');
                                var data = {
                                    name_user: req.body.name_user,
                                    last_name_user: req.body.last_name_user,
                                    birthday_user: birthday_user,
                                    photo: base64,//base64
                                    ext: ext_img,//.extencion
                                    ci_user: req.body.ci_user,
                                    email_account: req.body.email_account,
                                    app: "Nodo Academy",
                                    password_account: req.body.password_account
                                };
                                needle.post(USERSDB_SERVER + 'user/create', data, { "Content-Type": "application/json" }, function (err, resp) {
                                    if (err) {
                                        console.log(err);
                                        console.log('No se ha podido crear, por favor revise sus datos');
                                        //Evitar que la pagina de registro se quede cargando indefinidamente
                                        return done(null, false);
                                    } else {
                                        console.log(resp);
                                        console.log('Su cuenta ha sido creada');
                                        var mailOptions = {
                                            from: 'coopdbachita@gmail.com', //de
                                            to: resp.body.account.email_account, //para
                                            subject: 'Confirmar registro', //Asunto
                                            text: "Te damos la bienvenida al sistema HostPot de Nodo, Disfruta de los veneficios que te ofrecemos.\n\nGracias por registrarte en nuestra plataforma.\n\nTu usuario: " + req.body.email_account + "\nTu contraseña: " + req.body.password_account + "\n\nPor favor confirma tu correo electrónico en el siguiente enlace:\n\n" + THIS_DOMAIN + "verify?apikey=" + resp.body.account._id + "\n\n!Disfrutalos!\n" //contenido
                                        };
                                        //Enviar correo
                                        transporter.sendMail(mailOptions, function (error, info) {
                                            console.log("senMail returned!");
                                            if (error) {
                                                console.log("ERROR!!!!!!", error);
                                            } else {
                                                console.log('Email sent: ' + info.response);
                                            }
                                        });
                                        console.log('active_account', "Por favor Ingresa a tu correo y activa tu cuenta de NodoAcademy.");
                                        return done(null, resp.body.account);
                                    }
                                });
                                fs.unlinkSync(path_img);
                            });
                        } else {
                            var data = {
                                name_user: req.body.name_user,
                                last_name_user: req.body.last_name_user,
                                user_scol: req.body.user_scol,
                                gender: req.body.gender_user,
                                birthday_user: birthday_user,
                                photo: "",//base64
                                ext: "",//.extencion
                                ci_user: req.body.ci_user,
                                email_account: req.body.email_account,
                                app: "Nodo Academy",
                                password_account: req.body.password_account
                            };

                            needle.post(USERSDB_SERVER + 'user/create', data, { "Content-Type": "application/json" }, function (err, resp) {
                                if (err) {
                                    console.log('No se ha podido crear, por favor revise sus datos');
                                    //Evitar que la pagina de registro se quede cargando indefinidamente
                                    return done(null, false);
                                } else {
                                    console.log(resp);
                                    console.log('Su cuenta ha sido creada');
                                    var mailOptions = {
                                        from: 'coopdbachita@gmail.com', //de
                                        to: resp.body.account.email_account, //para
                                        subject: 'Confirmar registro', //Asunto
                                        text: "Te damos la bienvenida al sistema HostPot de Nodo, Disfruta de los veneficios que te ofrecemos.\n\nGracias por registrarte en nuestra plataforma.\n\nTu usuario: " + req.body.email_account + "\nTu contraseña: " + req.body.password_account + "\n\nPor favor confirma tu correo electrónico en el siguiente enlace:\n\n" + THIS_DOMAIN + "verify?apikey=" + resp.body.account._id + "\n\n!Disfrutalos!\n" //contenido
                                    };
                                    //Enviar correo
                                    transporter.sendMail(mailOptions, function (error, info) {
                                        console.log("senMail returned!");
                                        if (error) {
                                            console.log("ERROR!!!!!!", error);
                                        } else {
                                            console.log('Email sent: ' + info.response);
                                        }
                                    });
                                    //                                    req.flash('message', "Por favor Ingresa a tu correo y activa tu cuenta.");
                                    console.log(" registro exitoso.");
                                    return done(null, resp.body.account);
                                }
                            });
                        }
                    }
                } else {
                    console.log("Error:", error);
                    console.log('Servidor Fuera de Linea');
                    return done(null, null)
                }
            });
        }
    ));
}