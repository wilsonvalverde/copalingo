'use strict';

const Hostpod = require('../models/hostpod.model');
const Binnacle = require('../models/binnacle.model');

const HostpodController = {};

/**
 * @api {post} /api/hostpod/create Create a hotspot
 * @apiVersion 0.1.0
 * @apiGroup Hotspot
 * 
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost:4000/api/hostpod/create
 *  
 * Hotspot Params
 * @apiParam {String} number_hostpod
 * 
 */

HostpodController.saveHostpod = (req, res) => {
    const hostpod = new Hostpod(req.body);
    hostpod.save().then(hostpod => {
        if (hostpod) {
            var fecha = new Date();
            const dataBinnacle = {
                modific_date: fecha,
                description: "El Administrador " + req.session.name + " " + req.session.lastname + " a creado el hotspot " + req.body.number_hostpod
            }
            const binnacle = new Binnacle(dataBinnacle);
            binnacle.save();
            req.flash('GOOD', "El Hospot se ha guardado con éxito", "/dashboard/hotspot");
        }
        else req.flash('BAD', "No se ha podido guardar el registro", "/dashboard/hotspot");
    }).catch((err) => {
        console.log(err);
        req.flash('BAD', "Ha ocurrido un error al guardar el registro", "/dashboard/hotspot");
    });
}

module.exports = HostpodController;