'use strict';
const needle = require('needle');
const conn = require('../config/mikrotik/connection');

const Contract = require('../models/contract.model');
const Partner = require('../models/partner.model');
const Binnacle = require('../models/binnacle.model');

const ContractController = {};

/**
 * @api {post} /api/contract/create Create a contract
 * @apiVersion 0.1.0
 * @apiName Create
 * @apiGroup Contract
 * 
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost:4000/api/contract/create
 * 
 * @apiParam {String} number
 * @apiParam {String} duration
 * @apiParam {Date} start_date
 * @apiParam {Date} end_date
 * @apiParam {String} payment_method
 * @apiParam {Boolean} covenant
 * @apiParam {String} observation
 * @apiParam {String} reference
 * @apiParam {Boolean} status
 * @apiParam {ObjectId} service
 * @apiParam {ObjectId} client
 * @apiParam {ObjectId} issuance
 * 
 * @apiSuccess {String} msg
 * @apiSuccess {ObjectId} id_contract
 * @apiSuccess {Boolean} successful
 * 
 * @apiSuccessExample Success-Response:
 *      HTTP/1.1 200 OK
 *      {
 *          "msg": "Action completed",
 *          "id_contract": "5d0959b8213c0d39f858c5ac",
 *          "successful": true
 *      }
 * 
 * @apiError {String} msg
 * @apiError {Boolean} successful
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "msg": "<Error Message>",
 *       "successful": false
 *     }
 */
ContractController.saveContract = (req, res) => {
    Contract.findOne({ number: req.body.number })
        .then(response => {
            if (!response) {
                const contract = new Contract(req.body);
                contract.save().then(contract => {
                    if (contract) res.status(200).json({
                        msg: "Action completed",
                        id_contract: contract.id,
                        successful: true
                    });
                    else res.status(500).json({
                        msg: "Not saved",
                        successful: false
                    });
                }).catch((err) => {
                    console.log(err);
                    res.status(500).json({
                        msg: "Server Error",
                        successful: false
                    });
                });
            } else {
                res.status(200).json({
                    msg: "Duplicated",
                    successful: false
                });
            }
        }).catch((err) => {
            console.log(err);
            res.status(500).json({
                msg: "Server Error",
                successful: false
            });
        });
};

/**
 * @api {post} /api/contract/create-with-client Create a contract
 * @apiVersion 0.1.0
 * @apiName Create with Client
 * @apiGroup Contract
 * 
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost:4000/api/contract/create-with-client
 * 
 * Contract Params
 * @apiParam {String} number
 * @apiParam {String} duration
 * @apiParam {Date} start_date
 * @apiParam {Date} end_date
 * @apiParam {String} payment_method
 * @apiParam {Boolean} covenant
 * @apiParam {String} observation
 * @apiParam {String} reference
 * @apiParam {Boolean} status
 * @apiParam {ObjectId} service
 * @apiParam {ObjectId} client
 * @apiParam {ObjectId} issuance
 * 
 * Client Params
 * @apiParam {String} dni_type
 * @apiParam {String} dni
 * @apiParam {String} business_name
 * @apiParam {String} trade_name
 * @apiParam {String} person_type
 * @apiParam {String} client_type
 * @apiParam {String} profession
 * @apiParam {String} phone
 * @apiParam {String} email
 * @apiParam {String} country
 * @apiParam {String} city
 * @apiParam {String} address
 * @apiParam {String} parish
 * @apiParam {String} reference
 * @apiParam {Boolean} status
 * 
 * @apiError {String} error
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "error": "error",
 *     }
 * 
 */
ContractController.saveContractWithClient = (req, res) => {
    //Mikrotic
    conn.connect().then(() => {
        conn.write([
            "/ip/firewall/address-list/add",
            "=list=ACTIVADO",
            "=address=" + req.body.ip_service
        ]).then(() => {
            console.log("ADDRESS GUARDADA");
        }).catch(err => {
            console.log("ERROR AL GUARDAR");
        });
    });

    Partner.findOne({ dni: req.body.dni })
        .then(response => {
            if (!response) {
                Contract.findOne({ number: req.body.number })
                    .then(response => {
                        if (!response) {
                            var data_partner = {
                                dni_type: req.body.dni_type,
                                dni: req.body.dni,
                                business_name: req.body.business_name,
                                trade_name: req.body.trade_name,
                                person_type: req.body.person_type,
                                client_type: req.body.client_type,
                                profession: req.body.profession,
                                phone: req.body.phone,
                                email: req.body.email,
                                country: req.body.country,
                                city: req.body.city,
                                address: req.body.address,
                                parish: req.body.parish,
                                reference: req.body.reference,
                                status: req.body.status,

                            };
                            const partner = new Partner(data_partner);
                            partner.save().then(person => {
                                if (person) {
                                    console.log("Fecha contrato:");
                                    console.log(req.body.start_date);
                                    var data_contract = {
                                        number: req.body.number,
                                        duration: req.body.duration,
                                        start_date: req.body.start_date,
                                        end_date: req.body.end_date,
                                        payment_method: req.body.payment_method,
                                        covenant: req.body.covenant,
                                        observation: req.body.observation,
                                        reference: req.body.c_reference,
                                        status: req.body.c_status,
                                        ip_service: req.body.ip_service,
                                        mac_address: req.body.mac_address,
                                        number_hostpod: req.body.number_hostpod,
                                        status_hostpod: req.body.status_hostpod,
                                        service: req.body.service,
                                        client: person._id,
                                        issuance: req.body.issuance
                                    };
                                    const contract = new Contract(data_contract);
                                    contract.save().then(record => {
                                        if (record) {
                                            needle.get("http://201.182.151.33:8100/control/ip_binding/" + req.body.number_hostpod + "/bypassed/" + req.body.mac_address + "/" + req.body.ip_service, function (data, status) {
                                                console.log("Informacion Enviada");
                                            });

                                            // needle.post(process.env.URL_IMPERIUM + '/clientes/nuevo', data, {
                                            //     headers: {
                                            //         'Content-Type': 'application/json',
                                            //         'Authorization': 'Basic ' + process.env.TOKEN_IMPERIUM
                                            //     }
                                            // }, (err, resp) => {
                                            //     if (err) {
                                            //         // Server Ip Binding
                                            //         needle.get("http://201.182.151.33:8100/control/ip_binding/" + req.body.number_hostpod + "/bypassed/" + req.body.mac_address + "/" + req.body.ip_service, function (data, status) {
                                            //             console.log("Informacion Enviada");
                                            //         });
                                            //         return req.flash('BAD', "Los datos del cliente y contrato se han guardado pero ha ocurrido un error al intentar conectar con Imperium.", "/dashboard");
                                            //     }
                                            //     if (resp.body.Estado === 1) {
                                            //         // Server Ip Binding
                                            //         needle.get("http://201.182.151.33:8100/control/ip_binding/" + req.body.number_hostpod + "/bypassed/" + req.body.mac_address + "/" + req.body.ip_service, function (data, status) {
                                            //             console.log("Informacion Enviada");
                                            //         });
                                            //         
                                            //     } else {
                                            //         return req.flash('BAD', "Los datos del cliente y contrato se han guardado pero ha ocurrido un error al intentar guardar el registro en Imperium", "/dashboard");
                                            //     }
                                            // });
                                            return req.flash('GOOD', "El contrato se ha guardado con éxito", "/dashboard");
                                        } else {
                                            return req.flash('BAD', "Los datos del contrato no han sido almacenados.", "/dashboard");
                                        }
                                    }).catch((err) => {
                                        console.log(err);
                                        return req.flash('BAD', "Ha ocurrido un error al intentar guardar los datos del contrato", "/dashboard");
                                    });
                                } else {
                                    return req.flash('BAD', "Los datos del cliente no han sido almacenados.", "/dashboard");
                                }
                            }).catch((err) => {
                                console.log(err);
                                return req.flash('BAD', "Ha ocurrido un error al intentar guardar los datos del cliente", "/dashboard");
                            });
                        }
                    })
            } else {
                return req.flash('BAD', "El número de cédula ingresado ya existe.", "/dashboard/contract/new");
            }
        }).catch((err) => {
            console.log(err);
            req.flash('BAD', "Ha ocurrido un error al guardar el registro", "/dashboard/contract/new");
        });
}

/**
 * @api {post} /api/contract/create Suspend a contract
 * @apiVersion 0.1.0
 * @apiName Suspend contract
 * @apiGroup Contract
 * 
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost:4000/api/contract/suspend
 * 
 * @apiParam {String} number
 * @apiParam {String} reason
 * 
 * @apiError {String} error
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "error": "error",
 *     }
 */
ContractController.suspend = (req, res) => {
    var data = {
        Numero: req.body.number,
        Descripcion: req.body.reason
    };
    Contract.findOneAndUpdate({ number: req.body.number }, { $set: { status: false } })
        .then(() => {
            return req.flash('OK', "El contrato ha sido suspendido", "/dashboard");
        }).catch((err) => {
            console.log(err);
            req.flash('BAD', "Ha ocurrido un error al suspender el contrato", "/dashboard");
        });
};

/**
 * @api {post} /api/contract/create Activate a contract
 * @apiVersion 0.1.0
 * @apiName Activate contract
 * @apiGroup Contract
 * 
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost:4000/api/contract/activate
 * 
 * @apiParam {String} number
 * @apiParam {String} reason
 * 
 * @apiError {String} error
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "error": "error",
 *     }
 */
ContractController.activate = (req, res) => {
    var data = {
        Numero: req.body.number,
        Descripcion: req.body.reason
    };
    Contract.findOneAndUpdate({ number: req.body.number }, { $set: { status: true } })
        .then(() => {
            // var fecha = new Date();
            // const dataBinnacle = {
            //     modific_date: fecha,
            //     description: "El Administrador " + req.session.name + " " + req.session.lastname + " a activadp el punto de emision " + req.body.name
            // }
            // const binnacle = new Binnacle(dataBinnacle);
            // binnacle.save();

            return req.flash('OK', "El contrato ahora se encuentra activo", "/dashboard");
        }).catch((err) => {
            console.log(err);
            req.flash('BAD', "Ha ocurrido un error al reactivar el contrato", "/dashboard");
        });
};

/**
 * @api {get} /api/contract/all Get all contracts
 * @apiVersion 0.1.0
 * @apiName Get contracts
 * @apiGroup Contract
 * 
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost:4000/api/contract/all
 * 
 * @apiError {String} error
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "error": "error",
 *     }
 */
ContractController.getContracts = (req, res) => {
    Contract.find({ status: true })
        .populate('client')
        .populate('service')
        .exec()
        .then(response => {
            res.status(200).json(response);
        }).catch((err) => {
            console.log("ERROR MONGOOSE: " + err);
            res.status(500).json({ error: "error" });
        });
};

/**
 * @api {get} /api/contract/all Get a contract
 * @apiVersion 0.1.0
 * @apiName Get contract
 * @apiGroup Contract
 * 
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost:4000/api/contract/:id
 * 
 * @apiParam {String} id
 * 
 * @apiError {String} error
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "error": "error",
 *     }
 */
ContractController.getContract = (req, res) => {
    Contract.findById(req.params.id)
        .then(response => {
            res.status(200).json(response);
        }).catch((err) => {
            console.log(err);
            res.status(500).json({ error: "error" });
        });
};

/**
 * @api {get} /api/contract/search Search for a contract
 * @apiVersion 0.1.0
 * @apiName Search contract
 * @apiGroup Contract
 * 
 * @apiParam {String} search
 * 
 * @apiError {string} error
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "error": "error",
 *     }
 * 
 */
ContractController.search = (req, res) => {
    Contract.find({
        $or:
            [{ 'number': { $regex: '.*' + req.body.search + '.*' } },
            { 'ip_service': { $regex: '.*' + req.body.search + '.*' } }]
    })
        .populate('client')
        .exec()
        .then(response => {
            res.status(200).json(response);
        }).catch((err) => {
            console.log(err);
            res.status(500).json({ error: "error" });
        });
};

/**
 * @api {get} /api/contract/update/:id Search for a contract
 * @apiVersion 0.1.0
 * @apiName Search contract
 * @apiGroup Contract
 * 
 * @apiParam {string} :id
 * 
 * @apiParam {String} number
 * @apiParam {String} duration
 * @apiParam {Date} start_date
 * @apiParam {Date} end_date
 * @apiParam {String} payment_method
 * @apiParam {Boolean} covenant
 * @apiParam {String} observation
 * @apiParam {String} reference
 * @apiParam {Boolean} status
 * @apiParam {ObjectId} service
 * @apiParam {ObjectId} client
 * @apiParam {ObjectId} issuance
 * 
 * @apiError {string} error
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "error": "error",
 *     }
 */
ContractController.updateContract = (req, res) => {
    var numero = req.body.info;
    var id = req.params.id;
    var update = req.body;

    Contract.findByIdAndUpdate(id, update)
        .populate('client')
        .then(contractUpdate => {
            contractUpdate.save()
                .then(contractUpdate => {
                    var fecha = new Date();
                    var data_binnacle = {
                        modific_date: fecha,
                        description: "El contrato de " + contractUpdate.client.trade_name + " ha sido modificado."
                    };
                    const binnacle = new Binnacle(data_binnacle);
                    binnacle.save();

                    if (contractUpdate) {
                        needle.get("http://201.182.151.33:8100/control/edit/ip_binding/" + contractUpdate.number_hostpod + "/update/" + contractUpdate.status_hostpod + "/" + contractUpdate.mac_address + "/" + contractUpdate.ip_service + "/" + numero, function (data, status) {
                            console.log("Informacion Actualizada!");
                        });
                        req.flash('GOOD', "El contrato se ha modificado con éxito", "/dashboard");
                    } else req.flash('BAD', "No se ha podido modificar el contrato", "/dashboard");
                }).catch((err) => {
                    console.log(err);
                    req.flash('BAD', "Ha ocurrido un error al guardar el registro", "/dashboard");
                });
        });

};


/**
 * @api {get} /api/contract/delete/:id Search for a contract
 * @apiVersion 0.1.0
 * @apiName Search contract
 * @apiGroup Contract
 * 
 * @apiParam {string} :id
 * 
 * @apiParam {String} number
 * @apiParam {String} duration
 * @apiParam {Date} start_date
 * @apiParam {Date} end_date
 * @apiParam {String} payment_method
 * @apiParam {Boolean} covenant
 * @apiParam {String} observation
 * @apiParam {String} reference
 * @apiParam {Boolean} status
 * @apiParam {ObjectId} service
 * @apiParam {ObjectId} client
 * @apiParam {ObjectId} issuance
 * 
 * @apiError {string} error
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "error": "error",
 *     }
 */

ContractController.deleteContract = (req, res) => {
    var numero = req.body.info;

    Contract.findOne({ _id: req.params.id })
        .populate('client')
        .then(contractDelete => {
            contractDelete.delete()
                .then(contractD => {
                    var fecha = new Date();
                    var data_binnacle = {
                        modific_date: fecha,
                        description: "El contrato de " + contractUpdate.client.trade_name + " ha sido eliminado."
                    };
                    const binnacle = new Binnacle(data_binnacle);
                    binnacle.save();
                    if (contractD) {
                        needle.get("http://201.182.151.33:8100/control/edit/ip_binding/" + req.body.number_hostpod + "/delete/bypassed/" + req.body.mac_address + "/" + req.body.ip_service + "/" + numero, function (data, status) {
                            console.log("Informacion Eliminada!");
                        });
                        req.flash('GOOD', "El contrato se ha eliminado con éxito", "/dashboard");
                    } else req.flash('BAD', "No se ha podido eliminar el contrato", "/dashboard");
                }).catch((err) => {
                    console.log(err);
                    req.flash('BAD', "Ha ocurrido un error al eliminar el contrato", "/dashboard");
                });
        });



};

module.exports = ContractController;