const conn = require('../config/mikrotik/connection');

MikrotikController = {};

/**
 * EJEMPLO:
 * {
 *  "ip": 22.22.22.22,  <-- ahi mandas las ip a la que vas a cortar o reactivar el servicio
 *  "list": "CORTE" <-- ahi le cambias de list, sea de corte o de internet
 * }
 * 
 * la lista es la que define si un usuario tiene o no internet (o eso es lo que me exlicaron xd)
 */

 // esto ya esta implementado en el archivo de task, este archivo lo usaba unicamente para
MikrotikController.changeConnection = (req, res) => {
    conn.connect()
        .then(() => {
            conn.write('/ip/firewall/address-list/print', ['?address=' + req.body.ip]) // <-- aqui obtienes los datos mediante la ip, lo que necesitas es realmente el id para poder cambiar de lista
            .then(data => {
                if(data[0])
                    return conn.write('/ip/firewall/address-list/set', ['=.id=' + data[0]['.id'], '=list=' + req.body.list]) // <-- y aqui le fijas el valor de la lista 
                else
                    res.status(404).send({ error: "not found" });                              
            }).then(data => {
                conn.close();
                res.status(200).send(data);
            }).catch(err => {
                conn.close();
                console.log(err);
                res.status(500).send(err);
            });
        }).catch(err => {
            conn.close();
            console.log(err);
            res.status(500).send(err);
        });
}

MikrotikController.getIPs = (req, res) => {
    console.log(conn);
    conn.connect()
        .then(() => {
            conn.write('/ip/firewall/address-list/print')
            .then(data => {
                conn.close();
                res.status(200).send(data);
            }).catch(err => {
                conn.close();
                console.log(err);
                res.status(500).send(err);
            });
        });
}

module.exports = MikrotikController;