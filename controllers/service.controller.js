const needle = require('needle');

const Service = require('../models/service.model');
const Binnacle = require('../models/binnacle.model');

const ServiceController = {};

/**
 * @api {post} /api/service/create Create a service
 * @apiVersion 0.1.0
 * @apiGroup Service
 * 
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost:4000/api/service/create
 *  
 * Service Params
 * @apiParam {String} name
 * @apiParam {String} description
 * @apiParam {String} product
 * @apiParam {Date} invoice_date
 * @apiParam {String} type
 * @apiParam {Number} pricing
 * @apiParam {Boolean} status
 * 
 */

ServiceController.saveService = (req, res) => {

    Service.findOne({ code: req.body.name })
        .then(response => {
            if (!response) {
                const service = new Service(req.body);
                service.save().then(service => {
                    var fecha = new Date();
                    const dataBinnacle = {
                        modific_date: fecha,
                        description: "El Administrador " + req.session.name + " " + req.session.lastname + " a creado el servicio " + req.body.name
                    }
                    const binnacle = new Binnacle(dataBinnacle);
                    binnacle.save();

                    if (service) {
                        var data = {
                            Nombre: service.name,
                            FechaFacturacion: service.invoice_date,
                            CodigoProducto: req.body.product,
                            Valor: service.pricing
                        };
                        req.flash('GOOD', "El plan se ha guardado con éxito", "/dashboard/service");
                        
                    } else return req.flash('BAD', "No se ha podido guardar el plan", "/dashboard/service");
                }).catch((err) => {
                    console.log(err);
                    return req.flash('BAD', "Ha ocurrido un error al intentar guardar el plan", "/dashboard/service");
                });
            } else {
                return req.flash('OK', "El plan ya esta registrado", "/dashboard/service");
            }
        }).catch((err) => {
            console.log(err);
            return req.flash('BAD', "Ha ocurrido un error al intentar guardar el plan", "/dashboard/service");
        });
}

ServiceController.getServices = (req, res) => {
    Service.find()
        .then(response => {
            res.status(200).json(response);
        }).catch((err) => {
            console.log(err);
            res.status(500).json({ error: "error" });
        });
}

ServiceController.getService = (req, res) => {
    Service.findById(req.params.id)
        .then(response => {
            res.status(200).json(response);
        }).catch((err) => {
            console.log(err);
            res.status(500).json({ error: "error" });
        });
};

ServiceController.search = (req, res) => {
    Service.find({ $or: [{ 'code': { $regex: '.*' + req.body.search + '.*' } }, { 'name': { $regex: '.*' + req.body.search + '.*' } }] })
        .then(response => {
            res.status(200).json(response);
        }).catch((err) => {
            console.log(err);
            res.status(500).json({ error: "error" });
        });
}

/**
 * @api {post} /api/service/update/:id Update a service
 * @apiVersion 0.1.0
 * @apiGroup Service
 * 
 * @apiParam {string} :id
 *  
 * Service Params
 * @apiParam {String} name
 * @apiParam {String} description
 * @apiParam {String} product
 * @apiParam {Date} invoice_date
 * @apiParam {String} type
 * @apiParam {Number} pricing
 * @apiParam {Boolean} status
 * 
 */

ServiceController.updateService = (req, res) => {
    var id = req.params.id;
    Service.findByIdAndUpdate(id, req.body)
        .then(datosActualiza => {
            var fecha = new Date();
            const dataBinnacle = {
                modific_date: fecha,
                description: "El Administrador " + req.session.name + " " + req.session.lastname + " a modificado el servicio " + req.body.name
            }
            const binnacle = new Binnacle(dataBinnacle);
            binnacle.save();
            if (datosActualiza) req.flash('GOOD', "El Plan se ha modificado con éxito", "/dashboard/service");
            else req.flash('BAD', "No se ha podido modificar el registro", "/dashboard/service");
        }).catch((err) => {
            console.log(err);
            req.flash('BAD', "Ha ocurrido un error al modificar el registro", "/dashboard/service");
        });
};

/**
 * @api {post} /api/service/delete/:id Create a service
 * @apiVersion 0.1.0
 * @apiGroup Service
 * 
 * @apiParam {string} :id
 * 
 * Service Params
 * @apiParam {String} name
 * @apiParam {String} description
 * @apiParam {String} product
 * @apiParam {Date} invoice_date
 * @apiParam {String} type
 * @apiParam {Number} pricing
 * @apiParam {Boolean} status
 * 
 */

ServiceController.deleteService = (req, res) => {
    var id = req.params.id;
    Service.findOne({ _id: id })
        .then(service => {
            var fecha = new Date();
            const dataBinnacle = {
                modific_date: fecha,
                description: "El Administrador " + req.session.name + " " + req.session.lastname + " a eliminado el servicio " + req.body.name
            }
            const binnacle = new Binnacle(dataBinnacle);
            binnacle.save();
            service.delete();
            req.flash('GOOD', "El Plan se ha eliminado", "/dashboard/service");
        }).catch((err) => {
            req.flash('BAD', "Error al eliminar el Plan", "/dashboard/service");
        });
}

ServiceController.changeStatusService = (req, res) => {
    Service.findByIdAndUpdate(req.params.id, { status: false })
        .then(serviceUpdate => {
            if (serviceUpdate) res.status(200).json(serviceUpdate);
            else res.status(500).json({ error: "error" });
        }).catch((err) => {
            console.log(err);
            res.status(500).json({ error: "error" });
        });
};

module.exports = ServiceController;