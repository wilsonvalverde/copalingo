'use strict';
const Issuance = require('../models/issuance.model');
const Binnacle = require('../models/binnacle.model');

const IssuanceController = {};

/**
 * @api {post} /api/issuance/create Create a issuance
 * @apiVersion 0.1.0
 * @apiGroup Issuance
 * 
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost:4000/api/issuance/create
 *  
 * Issuance Params
 * @apiParam {String} name
 * @apiParam {String} description
 * @apiParam {String} cutting_day
 * 
 */

IssuanceController.saveIssuance = (req, res) => {
    const issuance = new Issuance(req.body);
    issuance.save().then(issuance => {
        if (issuance) {
            var fecha = new Date();
            const dataBinnacle = {
                modific_date: fecha,
                description: "El Administrador " + req.session.name + " " + req.session.lastname + " a creado el punto de emision " + req.body.name
            }
            const binnacle = new Binnacle(dataBinnacle);
            binnacle.save();
            req.flash('GOOD', "El Punto de Emisión se ha guardado con éxito", "/dashboard/issuance");
        }
        else req.flash('BAD', "No se ha podido guardar el registro", "/dashboard/issuance");
    }).catch((err) => {
        console.log(err);
        req.flash('BAD', "Ha ocurrido un error al guardar el registro", "/dashboard/issuance");
    });
}

/**
 * @api {post} /api/issuance/update/:id Update a issuance
 * @apiVersion 0.1.0
 * @apiGroup Issuance
 * 
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost:4000/api/issuance/update/:id
 *  
 * Issuance Params
 * @apiParam {String} name
 * @apiParam {String} description
 * @apiParam {String} cutting_day
 * 
 */

IssuanceController.updateIssuance = (req, res) => {
    var id = req.params.id;
    Issuance.findOne({ _id: id })
        .then(datosActualiza => {
            datosActualiza.cutting_day = req.body.cutting_day;
            datosActualiza.name = req.body.name;
            datosActualiza.description = req.body.description;

            datosActualiza.save()
                .then(datosActualiza => {
                    if (datosActualiza) {

                        var fecha = new Date();
                        const dataBinnacle = {
                            modific_date: fecha,
                            description: "El Administrador " + req.session.name + " " + req.session.lastname + " a modificado el punto de emision " + req.body.name
                        }
                        const binnacle = new Binnacle(dataBinnacle);
                        binnacle.save();

                        req.flash('GOOD', "El Punto de Emisión se ha modificado con éxito", "/dashboard/issuance");

                    } else req.flash('BAD', "No se ha podido guardar el registro", "/dashboard/issuance");
                }).catch((err) => {
                    console.log(err);
                    req.flash('BAD', "Ha ocurrido un error al guardar el registro", "/dashboard/issuance");
                });
        })
}

/**
 * @api {post} /api/issuance/delete/:id Delete a issuance
 * @apiVersion 0.1.0
 * @apiGroup Issuance
 * 
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost:4000/api/issuance/delete/:id
 *  
 * Issuance Params
 * @apiParam {String} name
 * @apiParam {String} description
 * @apiParam {String} cutting_day
 * 
 */

IssuanceController.deleteIssuance = (req, res) => {
    Issuance.findById({ _id: req.params.id })
        .then(issuance => {
            issuance.delete();
            req.flash('GOOD', "Punto de Emision Eliminado!", "/dashboard/issuance");
        }).catch((err) => {
            req.flash('BAD', "Error al eliminar el Punto de Emision!", "/dashboard/issuance");
        });
}

module.exports = IssuanceController;