const Admin = require('../models/admin.model');
const Binnacle = require('../models/binnacle.model');

const AuthController = {};

AuthController.registrarAdmin = (req, res) => {
    Admin.findOne({ dni: req.body.cedula })
        .then(response => {
            if (!response) {
                var dataAdminn = {
                    dni: req.body.cedula,
                    name: req.body.nombres,
                    last_name: req.body.apellidos,
                    email: req.body.correo,
                    password: req.body.password
                }

                const administrador = new Admin(dataAdminn);
                administrador.save().then(admin => {
                    if (admin) req.flash('GOOD', "Nuevo Administrado Registrado!", "/dashboard");
                    else req.flash('BAD', "Error al registrar!", "/dashboard");
                }).catch(err => {
                    console.log(err);
                    req.flash('BAD', "Ha ocurrido un error al guardar el registro", "/dashboard");
                });
            } else {
                req.flash('BAD', "Cédula ya registrada!", "/dashboard");
            }
        })
}

AuthController.iniciarSesion = (req, res) => {
    Admin.findOne({ email: req.body.username })
        .then(admin => {
            if (admin) {
                if (admin.password == req.body.pass) {
                    req.session.peluche = admin.name + " " + admin.last_name;
                    req.redirect('/dashboard');
                } else {
                    req.flash('BAD', "Usuario o Contraseña incorrectos!", "/dashboard");
                }
            } else {
                req.flash('BAD', "Administrador no registrado!", "/dashboard");
            }
        }).catch(err => {
            req.flash('BAD', "Error al obtener el administrador!", "/dashboard");
        });
}

AuthController.cerrarSesion = (req, res) => {
    console.log("CIERRA");
    var fecha = new Date();
    const dataBinnacle = {
        modific_date: fecha,
        description: "El Administrador " + req.session.name + " " + req.session.lastname + " a cerrado sesion."
    }
    const binnacle = new Binnacle(dataBinnacle);
    binnacle.save();
    req.flash('GOOD', "Ha salido del sistema!", "/dashboard");
    req.session.destroy();
}

module.exports = AuthController;