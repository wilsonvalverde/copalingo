const needle = require('needle');

const Payment = require('../models/payment.model');

const PaymentController = {};

PaymentController.addPayment = (req, res) => {
    Payment.findOne({ id_payment: req.body.id_payment })
    .then(payment => {
        if(req.body.balance <= payment.balance){
            var data = [{
                cuentasCobrar: {
                    id: payment.id_payment,
                    concepto: payment.concept,
                    fecha: payment.issue_date,
                    total: payment.total,
                    saldo: payment.balance
                },
                cobro: {
                    TipoCobro: req.body.type,
                    conceptoPago: "Cobro " + payment.concept,
                    abonoTotal: req.body.balance,
                    fechaEmision: payment.issue_date,
                    fechaCheque: new Date(),
                    numeroCheque: "",
                    numeroCuenta: "",
                    nombreBanco: "",
                    observacion: "",
                    fechaPago: new Date()
                },
                totalCobro: req.body.balance,
                saldo: payment.balance - req.body.balance
            }];
            console.log(data);
            needle.post(process.env.URL_IMPERIUM + "/contratos/activar", data, {
                headers: {
                    "Content-type": "application/json",
                    "Authorization": "Basic " + process.env.TOKEN_IMPERIUM
                }
            }, (err, resp) => {
                if(err) return req.flash('BAD',"Error al contactar con Imperium","/dashboard/payment");
                
                if(resp.body.Estado === 1){
                    payment.balance = payment.balance - req.body.balance;

                    if(payment.balance == 0){
                        payment.active = false;
                        payment.status = "Pagado";
                    }else{
                        payment.status = "Parcial";
                    }
                    payment.save().then(() => {
                        req.flash('GOOD',"El pago ha sido modificado","/dashboard/payment");
                    }).catch((err) => { 
                        console.log(err);
                        req.flash('BAD',"Ha ocurrido un error al actualizar el pago","/dashboard/payment");
                    });
                }else{
                    return req.flash('BAD',"Error en el servidor de Imperium","/dashboard/contract");
                }
            });         
        }else{
            req.flash('BAD',"El abono ingresado supera al valor de pago.","/dashboard/payment");
        }
    }).catch((err) => { 
        console.log(err);
        req.flash('BAD',"Ha ocurrido un error al obtener el pago","/dashboard/payment");
    });
};

module.exports = PaymentController;