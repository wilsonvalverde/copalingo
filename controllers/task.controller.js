'use strict';
const cron = require('node-cron');
const needle = require('needle');

const funciones = require('../public/js/funciones');
var fun = new funciones();
const Contract = require('../models/contract.model');
const Payment = require('../models/payment.model');

const conn = require('../config/mikrotik/connection');

const TaskController = {};
// Tarea para realizar Cortes (Directo a Mikrotik)
// minutos - horas - dia (mes) - mes - dia (semana)

const task = cron.schedule('* * * * *', () => {
    console.log("COPALINGO ==> RUNNING CUTTING TASK...");
    console.log("COPALINGO ==> Searching for active clients...");
    console.log("COPALINGO ==> Searching for debtor clients...");
    Contract.find({ debtor: true })
        .populate('client')
        .exec()
        .then(data => {
            var debtors = 0, clients = 0, errors = 0, done = 0;
            if (data.length != 0) {
                conn.connect().then(() => {
                    data.forEach(async (item) => {
                        clients = clients + 1;
                        debtors = debtors + 1;
                        console.log("COPALINGO ==> Debtor found: " + item.client.dni + " " + item.ip_service);
                        console.log("COPALINGO ==> Cutting service...");
                        conn.write('/ip/firewall/address-list/print', ['?address=' + item.ip_service])
                            .then(datos => {
                                if (datos[0]) {
                                    console.log("CORTANDO----------------------------------------------------------------------");
                                    return conn.write('/ip/firewall/address-list/set', ['=.id=' + datos[0]['.id'], '=list=CORTADO'])
                                        .then(dato => {
                                            done = done + 1;
                                            console.log("COPALINGO ==> DONE!");
                                        }).catch(err => {
                                            errors = errors + 1;
                                            console.log("COPALINGO ==> Error while attempting to cut service...");
                                        })
                                }
                            })
                    })
                }).catch(err => {
                    console.log("ERROR EN LA CONEXION")
                    console.log(err);
                });
            } else {
                Contract.find({ debtor: false })
                    .populate('client')
                    .exec()
                    .then(data => {
                        conn.connect().then(() => {
                            data.forEach(async (item) => {
                                clients = clients + 1;
                                debtors = debtors + 1;
                                console.log("COPALINGO ==> No Debtor found: " + item.client.dni + " " + item.ip_service);
                                console.log("COPALINGO ==> Activating service...");
                                conn.write('/ip/firewall/address-list/print', ['?address=' + item.ip_service])
                                    .then(datos => {
                                        if (datos[0]) {
                                            console.log("CORTANDO----------------------------------------------------------------------");
                                            return conn.write('/ip/firewall/address-list/set', ['=.id=' + datos[0]['.id'], '=list=ACTIVADO'])
                                                .then(dato => {
                                                    done = done + 1;
                                                    console.log("COPALINGO ==> DONE!");
                                                }).catch(err => {
                                                    errors = errors + 1;
                                                    console.log("COPALINGO ==> Error while attempting to active service...");
                                                })
                                        }
                                    })
                            })
                        }).catch(err => {
                            console.log("ERROR EN LA CONEXION")
                            console.log(err);
                        });
                    })
            }
        });


}, {
    scheduled: true,
    timezone: "America/Bogota"
});
task.stop();

// Tarea para Generar Pagos
// FORMATO: minutos - horas - dia (mes) - mes - dia (semana)
const payment_task = cron.schedule('59 11 1 * *', () => {
    console.log("COPALINGO ==> RUNNING PAYMENT TASK...");
    Contract.find({ status: true })
        .populate('client')
        .exec()
        .then(data => {
            data.forEach(item => {
                needle('get', process.env.URL_IMPERIUM + "/clientes/get/?identificacion=" + item.client.dni + "&login=ndcajero", {
                    headers: {
                        "Content-type": "application/json",
                        "Authorization": "Basic " + process.env.TOKEN_IMPERIUM
                    }
                }).then(resp => {
                    if (resp.body.Data) {
                        if (resp.body.Data.Cuentas.length > 0) {
                            console.log("COPALINGO ==> Debtor found: " + resp.body.Data.Identificacion);
                            var array = [], cont = 0;
                            resp.body.Data.Cuentas.forEach(cuenta => {
                                Payment.findOne({ id_payment: cuenta.Id })
                                    .then(payment => {
                                        cont = cont + 1;
                                        if (!payment) {
                                            var info = {
                                                id_payment: cuenta.Id,
                                                concept: cuenta.Concepto,
                                                issue_date: cuenta.Fecha,
                                                total: cuenta.Total,
                                                balance: cuenta.Saldo,
                                                status: cuenta.Estado,
                                                client: item.client.id
                                            };
                                            array.push(info);
                                        }
                                        if (cont === resp.body.Data.Cuentas.length && array.length > 0) {
                                            Payment.collection.insert(array, (err, docs) => {
                                                if (err) {
                                                    console.log("COPALINGO ==> ERROR WHILE TRYING TO SAVE THE PAYMENTS...");
                                                } else {
                                                    console.log("Payments Generated (" + resp.body.Data.Identificacion + ")");
                                                    if (!item.debtor) {
                                                        Contract.findOneAndUpdate(item.id, { $set: { debtor: true } }, (err, doc) => {
                                                            if (err) {
                                                                console.log("COPALINGO ==> ERROR WHILE TRYING TO UPDATE THE CONTRACT...");
                                                            } else {
                                                                console.log("COPALINGO ==> Contract updated...");
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        } else if (cont === resp.body.Data.Cuentas.length && array.length == 0) {
                                            console.log("COPALINGO ==> Nothing to generate...");
                                        }
                                    }).catch(err => {
                                        console.log("COPALINGO ==> ERROR WHILE TRYING TO OBTAIN THE PAYMENT DATA...");
                                    });
                            });
                        }
                    } else {
                        console.log("COPALINGO ==> Non-registered customer: " + item.client.dni);
                    }
                }).catch(err => {
                    console.log("COPALINGO ==> ERROR WHILE TRYING TO CONNECT WITH IMPERIUM...");
                });
            });
        }).catch(err => {
            console.log("COPALINGO ==> ERROR WHILE TRYING TO OBTAIN THE DATA...");
            console.log("COPALINGO ==> TASK FINISHED...");
        });
}, {
    scheduled: true,
    timezone: "America/Bogota"
});
payment_task.stop();

TaskController.startCuttingTask = (req, res) => {
    task.start();
    //res.status(200).send({ msg: "Cutting task started."});
}

TaskController.stopCuttingTask = (req, res) => {
    task.stop();
    //    res.status(200).send({ msg: "Cutting task stopped. "});
}

TaskController.startPaymentTask = (req, res) => {
    payment_task.start();
    res.status(200).send({ msg: "Payment task started." });
}

TaskController.stopPaymentTask = (req, res) => {
    payment_task.stop();
    res.send(200).send({ msg: "Payment task stopped." })
}

module.exports = TaskController;