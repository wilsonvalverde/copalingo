const Partner = require('../models/partner.model');

const PartnerController = {};

/**
 * @api {post} /api/partner/create Create a partner
 * @apiVersion 0.1.0
 * @apiGroup Partner
 * 
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost:4000/api/partner/create
 *  
 * Partner Params
 * @apiParam {String} dni_type
 * @apiParam {String} dni
 * @apiParam {String} business_name
 * @apiParam {String} trade_name
 * @apiParam {String} person_type
 * @apiParam {String} client_type
 * @apiParam {String} profession
 * @apiParam {String} phone
 * @apiParam {String} email
 * @apiParam {String} country
 * @apiParam {String} city
 * @apiParam {String} address
 * @apiParam {String} parish
 * @apiParam {String} reference
 * @apiParam {Boolean} status
 * 
 */

PartnerController.saveClient = (req, res) => {
    Partner.findOne({ dni: req.body.dni })
        .then(response => {
            if (!response) {
                const partner = new Partner(req.body);
                partner.save().then(record => {
                    if (record) res.status(200).json(record);
                    else res.status(500).json({ error: "error" });
                }).catch((err) => {
                    console.log(err);
                    res.status(500).json({ error: "error" });
                });
            } else {
                res.status(200).json({ error: "duplicated" });
            }
        }).catch((err) => {
            console.log(err);
            res.status(500).json({ error: "error" });
        });
}

PartnerController.getPartners = (req, res) => {
    Partner.find({ status: true })
        .then(response => {
            res.status(200).json(response);
        }).catch((err) => {
            console.log(err);
            res.status(500).json({ error: "error" });
        });
}

PartnerController.search = (req, res) => {
    Partner.find({ $or: [{ 'dni': { $regex: '.*' + req.body.search + '.*' } }, { 'business_name': { $regex: '.*' + req.body.search + '.*' } }] })
        .then(response => {
            res.status(200).json(response);
        }).catch((err) => {
            console.log(err);
            res.status(500).json({ error: "error" });
        });
}

PartnerController.getPartner = (req, res) => {
    Partner.findById(req.params.id)
        .then(response => {
            res.status(200).json(response);
        }).catch((err) => {
            console.log(err);
            res.status(500).json({ error: "error" });
        });
};

PartnerController.updatePartner = (req, res) => {
    Partner.findByIdAndUpdate(req.params.id, req.body)
        .then(partnerUpdate => {
            if (partnerUpdate) res.status(200).json(partnerUpdate);
            else res.status(500).json({ error: "error" });
        }).catch((err) => {
            console.log(err);
            res.status(500).json({ error: "error" });
        });
};

PartnerController.changeStatusPartner = (req, res) => {
    Partner.findByIdAndUpdate(req.params.id, { status: false })
        .then(partnerUpdate => {
            if (partnerUpdate) res.status(200).json(partnerUpdate);
            else res.status(500).json({ error: "error" });
        }).catch((err) => {
            console.log(err);
            res.status(500).json({ error: "error" });
        });
};


module.exports = PartnerController;