const mongoose = require('mongoose');
const { Schema } = mongoose;

const ContractSchema = new Schema({
    number: { type: String, required: true },
    duration:{ type:String,required:true },
    start_date: { type: Date, required: true },
    end_date: { type: Date, required: true },
    payment_method: {type:String, required: true},
    covenant: {type:Boolean,required:true},
    observation: { type: String },
    reference: { type: String },
    ip_service: {type: String},
    mac_address: {type: String},
    number_hostpod: {type: String},
    status_hostpod: {type: String},
    status: {type: Boolean, required:true},
    service: { type: Schema.Types.ObjectId, ref: 'Service' },
    client: { type: Schema.Types.ObjectId, ref: 'Partner' },    
    issuance: { type: Schema.Types.ObjectId, ref: 'Issuance' },
    hostpod: {type: Schema.Types.ObjectId, ref: 'Hostpod'},
    debtor: { type: Boolean, default: false }
});

module.exports = mongoose.model('Contract', ContractSchema);