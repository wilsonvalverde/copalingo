const mongoose = require('mongoose');
const { Schema } = mongoose;

const PartnerSchema = new Schema({
    dni_type: { type: String, required: true },
    dni: { type: String, required: true, unique: true },
    business_name: { type: String, required: true },
    trade_name: { type: String, required: true },
    person_type: { type: String, required: true },
    client_type: { type: String, required: true },
    profession: { type: String },
    phone: { type: String, required: true },
    email: { type: String, required: true},
    country: { type: String, required: true },
    city: { type: String, required: true },
    address: { type: String, required: true },
    parish: { type:String, required: true },
    reference: { type: String },
    status: { type: Boolean, default: true, default: true }
});

module.exports = mongoose.model('Partner', PartnerSchema);