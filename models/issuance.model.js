const mongoose = require('mongoose');
const { Schema } = mongoose;

const IssuanceSchema = new Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    cutting_day: { type: String, required: true }
}, { timestamps: false });

module.exports = mongoose.model('Issuance', IssuanceSchema);