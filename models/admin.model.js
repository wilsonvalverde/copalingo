const mongoose = require('mongoose');
const { Schema } = mongoose;

const AdminSchema = new Schema({
    dni: {type: String, required: true},
    name: {type:String, required: true},
    last_name: {type:String, required: true},
    email: {type:String, required: true},
    password: {type:String, required: true}
});

module.exports = mongoose.model('Admin', AdminSchema);