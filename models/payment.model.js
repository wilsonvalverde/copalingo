const mongoose = require('mongoose');
const { Schema } = mongoose;

const PaymentSchema = new Schema({
    id_payment: { type: Number, required: true },
    concept: { type: String, required: true },
    issue_date: { type: Date, required: true },
    total: { type: Number, required: true },
    balance: { type: Number, required: true },
    status: { type: String, required: true },
    client: { type: Schema.Types.ObjectId, ref: 'Partner' },
    active: { type: Boolean, default: true }
});

module.exports = mongoose.model('Payment', PaymentSchema);