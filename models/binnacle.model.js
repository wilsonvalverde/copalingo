const mongoose = require('mongoose');
const { Schema } = mongoose;

const BinnacleSchema = new Schema({
    modific_date: { type: Date, required: true },
    description: { type: String }       
});

module.exports = mongoose.model('Binnacle', BinnacleSchema);