const mongoose = require('mongoose');
const { Schema } = mongoose;

const ServiceSchema = new Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    product: { type: String, required: true },
    invoice_date: { type: Date, required: true },
    type: { type: String, required: true },
    pricing: { type: Number, required: true },
    status: { type: Boolean, required: true, default: true}
});

module.exports = mongoose.model('Service', ServiceSchema);