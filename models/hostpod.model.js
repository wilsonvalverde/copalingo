const mongoose = require('mongoose');
const { Schema } = mongoose;

const HospotSchema = new Schema({
    number_hostpod: { type: String, required: true }
});

module.exports = mongoose.model('Hostpod', HospotSchema);