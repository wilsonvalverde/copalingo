var express = require('express');
var createError = require('http-errors');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var path = require('path');
var flash = require('express-flash-notification');
const cors = require('cors');

require('dotenv').config();

const passport = require('passport');
const { Strategy } = require('passport-local');

var session = require('express-session');

var app = express();

//Setting
const db = require('./config/connection');

//const AuthController = require('./controllers/auth.controller');
//passport.use(new Strategy({ usernameField: 'email' }, AuthController.getSession));
//obj._id
//lert p=new Persona({
//  gfg:
//});
//p._id
/**
 *  Credenciales para la conexion
 */
global.USERSDB_SERVER = "http://201.182.151.33:8086/";
global.admins = ["jhon.carrion@unl.edu.ec", "davidruiz@electritelecom.com", "geantonxx@gmail.com", "epsm96@gmail.com"];
global.THIS_DOMAIN = "http://201.182.151.33:8102/";
global.mirror = [];

const flashOptions = {
  beforeSingleRender: function (item, callback) {
    if (item.type) {
      switch (item.type) {
        case 'GOOD':
          item.type = 'Hecho';
          item.alertClass = 'alert-success';
          break;
        case 'OK':
          item.type = 'Info';
          item.alertClass = 'alert-info';
          break;
        case 'BAD':
          item.type = 'Error';
          item.alertClass = 'alert-danger';
          break;
      }
    }
    callback(null, item);
  }
};

app.set('views', path.join(__dirname, 'resources'));
app.set('view engine', 'ejs');

//Middlewares
app.use(logger('dev'));
app.use(express.json());

app.use(session({
  name: 'example',
  secret: 'shuush',
  resave: false,
  saveUninitialized: true,
  cookie: {
    path: '/',
    httpOnly: true,
    secure: false,
    expires: 86400000
  },
}));

app.use(flash(app, flashOptions));

app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
require('./config/passport/passpor.js')(passport);
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});

app.use(cors());


//Routes
app.use('/api/admin', require('./routes/admin.routes'));
app.use('/api/partner', require('./routes/partner.routes'));
app.use('/api/service', require('./routes/service.routes'));
app.use('/api/contract', require('./routes/contract.routes'));
app.use('/api/mikrotik', require('./routes/mikrotik.routes'));
app.use('/api/issuance', require('./routes/issuance.routes'));
app.use('/api/payment', require('./routes/payment.routes'));
app.use('/api/task', require('./routes/task.routes'));
app.use('/api/hostpod', require('./routes/hostpod.routes'));

app.use('/dashboard', require('./routes/app.routes'));

// Error Handler
app.use(function (req, res, next) {
  next(createError(404));
});

app.use(function (err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.render('error');
});



module.exports = app;