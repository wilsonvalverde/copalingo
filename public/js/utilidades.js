function validarCedula(cedula) {
    var cad = cedula.trim();
    var total = 0;
    var longitud = cad.length;
    var longcheck = longitud - 1;
    var longruc = longitud - 3;

    if (cedula.length == 10){
        if (cad !== "" && longitud === 10) {
            for (i = 0; i < longcheck; i++) {
                if (i % 2 === 0) {
                    var aux = cad.charAt(i) * 2;
                    if (aux > 9)
                        aux -= 9;
                    total += aux;
                } else {
                    total += parseInt(cad.charAt(i)); // parseInt o concatenará en lugar de sumar
                }
            }
    
            total = total % 10 ? 10 - total % 10 : 0;
    
            if (cad.charAt(longitud - 1) == total) {
                return true;
            } else {
                return false;
            }
        }

    } else if (cedula.length == 13) {
        if (cad !== "" && longitud === 10) {
            for (i = 0; i < longcheck; i++) {
                if (i % 2 === 0) {
                    var aux = cad.charAt(i) * 2;
                    if (aux > 9)
                        aux -= 9;
                    total += aux;
                } else {
                    total += parseInt(cad.charAt(i)); // parseInt o concatenará en lugar de sumar
                }
            }
    
            total = total % 10 ? 10 - total % 10 : 0;
    
            if (cad.charAt(longitud - 1) == total) {
                return true;
            } else {
                return false;
            }
        }
    }
    
}

function convert(fecha1) {
    var date = new Date(fecha1),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
    var fecha = ([date.getFullYear(), mnth, day].join("-"));
    // $("#fecha").val(fecha);
    return fecha;
}

function ConvertirFecha(fecha) {
    
    console.log("fecha a transformar:"+fecha);
    var hoy = new Date(fecha);
    var dia = ContarCero(hoy.getDate(), 2);
    var mes = ContarCero((hoy.getMonth() + 1), 2);

    if (dia < 10){
        dia = '0'+dia;
    }
    if (mes < 10){
        mes = '0'+mes;
    }

    var f = (hoy.getFullYear()+ '-' + mes + '-' + dia );
    return f;

}


function ContarCero(texto, nCero) {
    texto = texto + "";
    var longi = texto.lenth;
    var aux = nCero - longi;
    var cadena = "";
    if (aux < nCero) {
        for (var i = longi; i < nCero; i++) {
            cadena += '0';
        }
        cadena += texto;
        return cadena;
    } else {
        return texto;
    }
}

function monthDiff(f1, f2) {
    var d1 = new Date(f1);
    var d2 = new Date(f2);
    var months;
    console.log("Entra");
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth() + 1;
    months += d2.getMonth();

    var diasdif= d2.getTime()-d1.getTime();
    var contdias = Math.round(diasdif/(1000*60*60*24));
    
    var meses = months <= 0 ? 0 : months;
    return (meses+" meses y "+ contdias+" días.");
}