'use strict';

class funciones {

    mes(corte) {
        var hoy = new Date();
        var dd = corte;
        var mm = hoy.getMonth() + 1;
        var yyyy = hoy.getFullYear();


        return (mm);
    }

    diasemana(corte) {
        var hoy = new Date();

        var mes = hoy.getMonth() + 1;
        var dia = corte;
        var anio = hoy.getFullYear();
        var dias = ["dom", "lun", "mar", "mie", "jue", "vie", "sab"];
        var dt = new Date(mes + ' ' + dia + ', ' + anio);
        var text = "Dia de la semana : " + dias[dt.getUTCDay()];
        var hoy = dt.getUTCDay();
        console.log("Hola: " + text + " " + dt.getUTCDay())
        return (hoy);
    }

    fechaCorte(dato) {
        var hoy = new Date();
        var dd = dato;
        var mm = hoy.getMonth() + 1;
        var yyyy = hoy.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        return (yyyy + '-' + mm + '-' + dd);
    }

    fechaHoy() {

        var hoy = new Date();
        var dd = hoy.getDate();
        var mm = hoy.getMonth() + 1;
        var yyyy = hoy.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        return (yyyy + '-' + mm + '-' + dd);
    }


    ConvertirFecha(dato) {
        var fecha = dato;
        var hoy = new Date(fecha);
        var dia = ContarCero(hoy.getDate(), 2);
        var mes = ContarCero((hoy.getMonth() + 1), 2);

        if (dia < 10) {
            dia = '0' + dia;
        }
        if (mes < 10) {
            mes = '0' + mes;
        }

        var f = (hoy.getFullYear() + '-' + mes + '-' + dia);

        return f;

    }
    
}

module.exports = funciones;