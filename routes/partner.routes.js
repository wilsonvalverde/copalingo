const express = require('express');
const router = express.Router();

const PartnerController = require('../controllers/partner.controller');

router.get('/all', PartnerController.getPartners);
router.get('/:id',PartnerController.getPartner)
router.post('/create', PartnerController.saveClient);
router.post('/search',PartnerController.search);
router.put('/update/:id',PartnerController.updatePartner);
router.put('/delete/:id',PartnerController.changeStatusPartner);


module.exports = router;