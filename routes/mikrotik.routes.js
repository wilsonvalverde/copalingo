const express = require('express');
const router = express.Router();

const MikrotikController = require('../controllers/mikrotik.controller');

router.get('/ips', MikrotikController.getIPs);
router.post('/shutdown', MikrotikController.changeConnection);

module.exports = router;