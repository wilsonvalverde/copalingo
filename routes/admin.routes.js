const express = require('express');
const router = express.Router();

const AdminController = require('../controllers/auth.controller');

router.post('/guardarAdmin', AdminController.registrarAdmin);
router.post('/iniciarSesion', AdminController.iniciarSesion);
router.get('/cerrarSesion', AdminController.cerrarSesion);
// router.post('/guardarIP', ContractController.guardarIP);
//router.put('/delete/:id',ContractController.changeStatusContract); 
// router.post('/delete/:id', ContractController.deleteContract);
module.exports = router;