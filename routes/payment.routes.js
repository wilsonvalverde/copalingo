const express = require('express');
const router = express.Router();

const PaymentController = require('../controllers/payment.controller');

router.post('/add', PaymentController.addPayment);


module.exports = router;