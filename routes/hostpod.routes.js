const express = require('express');
const router = express.Router();

const HostpodController = require('../controllers/hostpod.controller');

router.post('/create', HostpodController.saveHostpod);
//router.post('/update/:id', IssuanceController.updateIssuance);

module.exports = router;