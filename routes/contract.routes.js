const express = require('express');
const router = express.Router();

const ContractController = require('../controllers/contract.controller');

router.get('/all', ContractController.getContracts);
router.get('/:id', ContractController.getContract);
router.post('/create', ContractController.saveContract);
router.post('/create-with-client', ContractController.saveContractWithClient);
router.post('/search', ContractController.search);
router.post('/suspend', ContractController.suspend); 
router.post('/activate', ContractController.activate);
router.post('/update/:id',ContractController.updateContract);
// router.post('/guardarIP', ContractController.guardarIP);
//router.put('/delete/:id',ContractController.changeStatusContract); 
router.post('/delete/:id', ContractController.deleteContract);
module.exports = router; 