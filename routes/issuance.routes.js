const express = require('express');
const router = express.Router();

const IssuanceController = require('../controllers/issuance.controller');

router.post('/create', IssuanceController.saveIssuance);
router.post('/update/:id', IssuanceController.updateIssuance);
router.post('/delete/:id', IssuanceController.deleteIssuance);

module.exports = router;