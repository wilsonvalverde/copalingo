const express = require('express');
const router = express.Router();

const ServiceController = require('../controllers/service.controller');

router.get('/all', ServiceController.getServices);
router.get('/:id', ServiceController.getService);
router.post('/create', ServiceController.saveService);
router.post('/delete/:id', ServiceController.deleteService)
router.post('/search', ServiceController.search);
router.post('/update/:id',ServiceController.updateService);
// router.put('/delete/:id',ServiceController.changeStatusService);
module.exports = router;