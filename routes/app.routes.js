const express = require('express');
const router = express.Router();
const needle = require('needle');
const conn = require('../config/mikrotik/connection');
const passport = require('passport');

const task = require('../controllers/task.controller');
const funciones = require('../public/js/funciones');
var fun = new funciones();

const adminFunciones = require('../controllers/auth.controller');

const ServiceModel = require('../models/service.model');
const IssuanceModel = require('../models/issuance.model');
const ContractModel = require('../models/contract.model');
const PaymentModel = require('../models/payment.model');
const PartnerModel = require('../models/partner.model');
const HostpodModel = require('../models/hostpod.model');
const Binnacle = require('../models/binnacle.model');

/*
*validar session
*/

var auth = function (req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        req.flash('BAD', "Debes iniciar sesion !", "/dashboard");
    }
}



/**
 *  Credenciales para la conexion
 */
global.USERSDB_SERVER = "http://201.182.151.33:8086/";
global.admins = ["jhon.carrion@unl.edu.ec", "davidruiz@electritelecom.com", "geantonxx@gmail.com", "epsm96@gmail.com"];
global.THIS_DOMAIN = "http://201.182.151.33:8102/";
global.mirror = [];

/**
 * Convertir Excel a Json
 */

const mongoose = require("mongoose");
//let url = "mongodb://localhost:27017/youtube";
mongoose.connect('mongodb://localhost:27017/erp');
var db = mongoose.connection;
// var Partner = mongoose.model('Partner', PartnerSchema, 'partners');

var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");

var bodyParser = require('body-parser');
var multer = require('multer');
router.use(bodyParser.json());

var storage = multer.diskStorage({
    // destination: function (req, file, cb) {
    //   cb(null, './uploads')
    // },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
    }
});
var upload = multer({ //multer settings
    storage: storage,
    fileFilter: function (req, file, callback) { //file filter
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(new Error('Wrong extension type'));
        }
        callback(null, true);
    }
}).single('file');

router.post('/upload', auth, function (req, res) {
    var exceltojson;
    upload(req, res, function (err) {
        if (err) {
            req.flash('BAD', 'Error al convertir Excel', "/dashboard/convert");
            //   res.json({ error_code: 1, err_desc: err });
            return;
        }
        /** Multer gives us file info in req.file object */
        if (!req.file) {
            req.flash('BAD', 'Seleccione un archivo Excel', "/dashboard/convert");
            //   res.json({ error_code: 1, err_desc: "No file passed" });
            return;
        }
        /** Check the extension of the incoming file and
         *  use the appropriate module
         */
        if (req.file.originalname.split('.')[req.file.originalname.split('.').length - 1] === 'xlsx') {
            exceltojson = xlsxtojson;
        } else {
            exceltojson = xlstojson;
        }

        var arreglo = req.file.originalname.split('.');

        try {
            exceltojson({
                input: req.file.path,
                output: null, //since we don't need output.json
                lowerCaseHeaders: true
            }, function (err, result, a) {
                if (err) {
                    req.flash('BAD', 'Error al convertir a JSON', "/dashboard/convert");
                    //   return res.json({ error_code: 1, err_desc: err, data: null });
                }



                result.forEach(dato => {
                    console.log(dato);
                    var id_cliente = "";
                    var id_service = "";
                    var id_issuance = "";
                    var id_hostpod = "";

                    if (dato.dni != "") {

                        var cliente = new PartnerModel({
                            dni_type: dato.tipo_dni,
                            dni: dato.dni + "",
                            business_name: dato.razon_social,
                            trade_name: dato.nombre,
                            person_type: dato.tipo_persona,
                            client_type: dato.tipo_cliente,
                            profession: dato.profesion,
                            phone: dato.telefono,
                            email: dato.correo,
                            country: dato.pais,
                            city: dato.ciudad,
                            address: dato.direccion,
                            parish: dato.parroquia,
                            reference: dato.referencia_cliente,
                            // Preguntar estatus
                            status: true
                        });

                        PartnerModel.collection.insert(cliente, function (err, res) {
                            if (err) {
                                return console.error(err);
                            }
                            id_cliente = res.ops[0]._id;
                            insertat(id_cliente);
                        });

                        
                        

                        function insertat(id) {
                            console.log(id);

                            var servicio = new ServiceModel({
                                name: dato.nombre_servicio,
                                description: dato.descripcion_servicio,
                                product: dato.numero_servicio,
                                invoice_date: dato.fecha_servicio,
                                type: dato.tipo_servicio,
                                pricing: dato.precio,
                                status: true
                            });

                            ServiceModel.collection.insert(servicio, function (err, res) {
                                if (err) {
                                    return console.error(err);
                                }
                                id_service = res.ops[0]._id;
                                idServicio(id, id_service);
                            });

                            
                        }

                        function idServicio(idUser, idSerer) {
                            var issuance = new IssuanceModel({
                                name: dato.punto_emision,
                                description: dato.descripcion_punto,
                                cutting_day: dato.dia_corte
                            });

                            IssuanceModel.collection.insert(issuance, function (err, res) {
                                if (err) {
                                    return console.error(err);
                                }
                                id_issuance = res.ops[0]._id;
                                idIssuance(idUser, idSerer, id_issuance);

                            });
                        }

                        function idIssuance(idcli, idserv, id_iss) {
                            var contrato = new ContractModel({
                                number: dato.numero_contrato,
                                duration: dato.duracion,
                                start_date: dato.fecha_inicio,
                                end_date: dato.fecha_fin,
                                payment_method: dato.metodo_pago,
                                //Preguntar booleano
                                covenant: dato.convenio,
                                observation: dato.observacion_contrato,
                                reference: dato.referencia_contrato,
                                ip_service: dato.ip,
                                mac_address: dato.mac,
                                number_hostpod: dato.hostpod_contrato,
                                status_hostpod: dato.estado_hostpod,
                                status: true,
                                debtor: false,
                                service: idserv,
                                client: idcli,
                                issuance: id_iss,
                            });

                            ContractModel.collection.insert(contrato, function (err, res) {
                                if (err) {
                                    return console.error(err);
                                }
                            });
                        }

                    } else {
                        req.flash('BAD', "FALTAN CEDULAS DE CLIENTES", "/dashboard/convert");
                    }

                });


                req.flash('GOOD', 'ARCHIVO SUBIDO A LA BASE DE DATOS', "/dashboard/convert");
                // res.redirect('http://localhost:4000/dashboard/convert');
                // crear archivo json
            });
        } catch (e) {
            req.flash('BAD', 'Seleccione un archivo Excel', "/dashboard/convert");
            //   res.json({ error_code: 1, err_desc: "Corupted excel file" });
        }
    })
});
/**
 * Fin
 */


router.get('/', (req, res) => {
    //    task.startCuttingTask();
    var search = req.body.search;
        console.log(req);
    if (req.isAuthenticated()) {

        

        ContractModel.find()
            .populate('client')
            .populate('service')
            .then(contracts => {
                res.render('views/dashboard_view', {
                    sesion: true,
                    name: req.session.name,
                    last: req.session.lastname,
                    contracts: contracts,
                    searchs: req.body.search
                });
            });
    } else {
        res.render('views/dashboard_view', { sesion: false });
    }
});
router.get('/buscar', (req, res) => {
    //    task.startCuttingTask();
    if (req.isAuthenticated()) {
        ContractModel.find()
            .populate('client')
            .populate('service')
            .then(contracts => {
                res.render('views/dashboard_view', {
                    sesion: true,
                    name: req.session.name,
                    last: req.session.lastname,
                    contracts: contracts
                });
            });
    } else {
        res.render('views/dashboard_view', { sesion: false });
    }
});

router.get('/partner', auth, (req, res) => {
    //task.stopCuttingTask();

    ContractModel.find({ debtor: true })
        .populate('client')
        .populate('service')
        .exec()
        .then(contracts => {
            res.render('views/partner_list', {
                contracts: contracts,
                name: req.session.name,
                last: req.session.lastname
            });
        });
});
//////////////////////////CONTRACTS
router.get('/contract/delete/:id/:ip', auth, (req, res) => {
    var info;
    var numero;
    ContractModel.find({ _id: req.params.id })
        .populate('client')
        .populate('issuance')
        .populate('service')
        .exec()
        .then(contracts => {
            needle.get("http://201.182.151.33:8100/control/get/3809/ip-binding/" + req.params.ip, function (data, status) {
                info = status.body;
                console.log(info);
                info.data.forEach(function (item, index) {
                    if (item.address == req.params.ip) {
                        numero = item.numbers;
                        console.log("numero ELIMINADO: " + numero);
                    }
                });
                res.render('views/contract_delete', {
                    contracts: contracts,
                    info: numero,
                    name: req.session.name,
                    last: req.session.lastname
                });
            });
        });
});

router.get('/contract/modific/:id/:ip', auth, (req, res) => {
    var info;
    var numero;
    ContractModel.find({ _id: req.params.id })
        .populate('client')
        .populate('issuance')
        .populate('service')
        .exec()
        .then(contracts => {
            needle.get("http://201.182.151.33:8100/control/get/3809/ip-binding/" + req.params.ip, function (data, status) {
                info = status.body;
                console.log(info);
                info.data.forEach(function (item, index) {
                    if (item.address == req.params.ip) {
                        numero = item.numbers;
                        console.log("numero: " + numero);
                    }
                });
                res.render('views/contract_modific', {
                    contracts: contracts,
                    info: numero,
                    name: req.session.name,
                    last: req.session.lastname
                });
            });
        });
});

router.get('/contract/new', auth, (req, res) => {
    ServiceModel.find()
        .then(services => {
            IssuanceModel.find()
                .then(issuances => {
                    HostpodModel.find()
                        .then(hostpod => {
                            res.render('views/contract_new', {
                                services: services,
                                issuances: issuances,
                                hostpod: hostpod,
                                name: req.session.name,
                                last: req.session.lastname
                            });
                        })
                });
        });
});

//////////////////////////// CONTRACTS

router.get('/issuance/modific/:id', auth, (req, res) => {
    IssuanceModel.find({ _id: req.params.id })
        .then(issuances => {
            res.render('views/issuance_modific', {
                issuances: issuances,
                name: req.session.name,
                last: req.session.lastname
            });
        });
});

router.get('/issuance/delete/:id', auth, (req, res) => {
    IssuanceModel.find({ _id: req.params.id })
        .then(issuances => {
            res.render('views/issuance_delete', {
                issuances: issuances,
                name: req.session.name,
                last: req.session.lastname
            });
        });
});

router.get('/issuance', auth, (req, res) => {
    IssuanceModel.find()
        .then(issuances => {
            res.render('views/issuance_list', {
                issuances: issuances,
                name: req.session.name,
                last: req.session.lastname
            });
        });
});
router.get('/issuance/new', auth, (req, res) => {
    res.render('views/issuance_new', {
        name: req.session.name,
        last: req.session.lastname
    });
});

//////////////////////////////// ISSUANCES FIN

router.get('/service/modific/:id', auth, (req, res) => {
    ServiceModel.find({ _id: req.params.id })
        .then(service => {
            res.render('views/service_modific', {
                service: service,
                name: req.session.name,
                last: req.session.lastname
            });
        });
});

router.get('/service/delete/:id', auth, (req, res) => {
    ServiceModel.find({ _id: req.params.id })
        .then(service => {
            res.render('views/service_delete', {
                service: service,
                name: req.session.name,
                last: req.session.lastname
            });
        });
});

router.get('/service', auth, (req, res) => {
    ServiceModel.find()
        .then(services => {
            res.render('views/service_list', {
                services: services,
                name: req.session.name,
                last: req.session.lastname
            });
        });
});
router.get('/service/new', auth, (req, res) => {
    res.render('views/service_new', {
        name: req.session.name,
        last: req.session.lastname
    });
});

///////////////////////////////// SERVICES FIN

router.get('/payment', auth, (req, res) => {

    ContractModel.find({ debtor: true })
        .populate('client')
        .then(deudores => {
            res.render('views/payment_list', {
                payments: deudores,
                name: req.session.name,
                last: req.session.lastname
            });
        });

    // PaymentModel.find({ active: true })
    //     .populate('client')
    //     .exec()
    //     .then(payments => {
    //         var data = [];
    //         payments.forEach(payment => {
    //             let aux = payment.toObject();
    //             aux.issue_date = aux.issue_date.getFullYear() + "/" + (aux.issue_date.getMonth() + 1) + "/" + aux.issue_date.getDate() + " " + aux.issue_date.getHours() + ":" + aux.issue_date.getMinutes() + ":" + aux.issue_date.getSeconds();
    //             data.push(aux);
    //         });
    //         res.render('views/payment_list', {
    //             payments: data
    //         });
    //     });
});

////////////////////////// PAYMENT FIN

router.get('/convert', auth, (req, res) => {
    res.render('views/ExcelTOJson', {
        name: req.session.name,
        last: req.session.lastname
    });
});

//////////////////////////////// CONVERT EXCEL TO JSON

router.get('/hotspot', auth, (req, res) => {
    HostpodModel.find()
        .then(hostpod => {
            res.render('views/hostpod_list', {
                hostpod: hostpod,
                name: req.session.name,
                last: req.session.lastname
            });
        });
});

router.get('/hotspot/new', auth, (req, res) => {
    res.render('views/hostpod_new', {
        name: req.session.name,
        last: req.session.lastname
    });
});

////////////////////////////// HOTSPOTS FIN

router.get('/binnacle', auth, (req, res) => {
    Binnacle.find().
        then(binnacle => {
            res.render('views/binnacle_list', {
                binnacles: binnacle,
                name: req.session.name,
                last: req.session.lastname
            });
        });
});


router.get('/register', auth, (req, res) => {
    res.render('views/register', {
        name: req.session.name,
        last: req.session.lastname
    })
});


////////////////// BITACORA FIN

router.get('/cerrarSesion', adminFunciones.cerrarSesion);


/**
 * Registro
 */
router.post('/save', passport.authenticate('local-signup', {
    successRedirect: '/users',
    failureRedirect: '/users/register',
}));

/**
 * Login
 */
router.post('/auth', passport.authenticate('local-signin', {
    successRedirect: '/dashboard',
    failureRedirect: '/dashboard'
}));

module.exports = router;