const express = require('express');
const router = express.Router();

const TaskController = require('../controllers/task.controller');

router.post('/startcuts', TaskController.startCuttingTask);
router.post('/stopcuts', TaskController.stopCuttingTask);
router.post('/startpayment', TaskController.startPaymentTask);

module.exports = router;